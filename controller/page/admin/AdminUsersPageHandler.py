from controller.page import BasePageHandler, web_auth, admin_auth
from tornado import gen
from tornado.web import asynchronous


class AdminUsersPageHandler(BasePageHandler):

    @web_auth
    @admin_auth
    @asynchronous
    @gen.coroutine
    def get(self):
        users = self.settings['db'].User.find()

        count = yield users.count()
        result = yield users.to_list(count)

        self.render('users_table_for_admin.html', users=result, user=self.user)
